-- DESCRIPTION --

Block Slider is another "slideshow" module for Drupal 7.

Its source is a port of the project "Featured Content Slider" (http://drupal.org/project/content_slider) which has no version for Drupal 7.

Some new features are added :
- support of multiple slideshows on a same page
- more parameters in settings
...


-- REQUIREMENTS --

None


-- INSTALLATION --
1. Extract the Block Slider module tarball and place the entire block_slider
  directory into your Drupal setup (e.g. in sites/all/modules).

2. Enable this module as any other Drupal module by navigating to
  administer > site building > modules


- CONFIGURATION --

* Configure user permissions in Administration � Configuration � User interface � Slideshows :

  - access block_slider

    Users in roles with the "Access block_slider" permission will see the slideshows.

  - administer block_slider

    Users in roles with the "Administer block_slider" permission will manage the slideshows.
    
* Customize the blocks of slideshows in Administration � Structure � Blocks.

  
-- CONTACT --

Current maintainers:
* Nicolas PALMIER - http://drupal.org/user/1458246
